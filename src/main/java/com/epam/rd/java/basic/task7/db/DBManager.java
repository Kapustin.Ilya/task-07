package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;
	Connection connection;

	public static synchronized DBManager getInstance() {
		DBManager newDBManager = new DBManager();
		try{
			newDBManager.connection = DriverManager.getConnection("jdbc:derby:memory:testdb;create=true");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return newDBManager;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		List<User> allUser = new ArrayList<User>();
		try {
			Statement stmt = connection.createStatement();
			ResultSet resultSet = stmt.executeQuery("SELECT * FROM users");
			while (resultSet.next()){
				User nextUser = User.createUser(resultSet.getString(2));
				nextUser.setId(resultSet.getInt(1));
				allUser.add(nextUser);
			}
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return allUser;
	}

	public boolean insertUser(User user) throws DBException {
		String sql
				= "INSERT INTO users(login) VALUES (?)";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1,user.getLogin());
			preparedStatement.executeUpdate();

			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			if(resultSet.next()) {
				int key = resultSet.getInt(1);
				user.setId(key);
				preparedStatement.close();
				return true;
			} else {
				preparedStatement.close();
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public User getUser(String login) throws DBException {
		List<User> allUsers = this.findAllUsers();
		for (User temp : allUsers){
			if (temp.getLogin().equals(login)) {
				return temp;
			}
		}
		return null;
	}

	public Team getTeam(String name) throws DBException {
		List<Team> allTeams = this.findAllTeams();
		for (Team temp : allTeams){
			if (temp.getName().equals(name)) {
				return temp;
			}
		}
		return null;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> allTeam = new ArrayList<Team>();
		try {
			Statement stmt = connection.createStatement();
			ResultSet resultSet = stmt.executeQuery("SELECT * FROM teams");
			while (resultSet.next()){
				Team nextTeam = Team.createTeam(resultSet.getString(2));
				nextTeam.setId(resultSet.getInt(1));
				allTeam.add(nextTeam);
			}
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return allTeam;
	}

	public boolean insertTeam(Team team) throws DBException {
		String sql
				= "INSERT INTO teams(name) VALUES (?)";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			preparedStatement.setString(1,team.getName());
			preparedStatement.executeUpdate();

			ResultSet resultSet = preparedStatement.getGeneratedKeys();
			if(resultSet.next()) {
				int key = resultSet.getInt(1);
				team.setId(key);
				preparedStatement.close();
				return true;
			} else {
				preparedStatement.close();

			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {

		try {
            connection.setAutoCommit(false);
            connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			String sql
					= "INSERT INTO users_teams VALUES (?,?)";
			PreparedStatement preparedStatement = connection.prepareStatement(sql, Statement.NO_GENERATED_KEYS);

			for (Team temp : teams){
				preparedStatement.setInt(1, user.getId());
				preparedStatement.setInt(2, temp.getId());
				preparedStatement.executeUpdate();
			}
			connection.commit();
			preparedStatement.close();
			return true;
		} catch (SQLException e) {
			try {
				connection.rollback();
				connection.setAutoCommit(true);
				throw new DBException("a transaction has been failed", e);
			} catch (SQLException ex) {
				throw new DBException("a transaction has been failed", ex);
			}
		} finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> userTeam = new ArrayList<>();
		try {
			List<Team> allTeams = this.findAllTeams();
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM users_teams");
			while (resultSet.next()){
				int intUser = resultSet.getInt(1);
				if (intUser == user.getId()){
					int intTeam = resultSet.getInt(2);
					for (Team temp : allTeams) {
						if (temp.getId() == intTeam){
							userTeam.add(temp);
							break;
						}
					}
				}
			}
			resultSet.close();
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userTeam;

	}

	public boolean deleteTeam(Team team) throws DBException {
		String sql = "DELETE FROM teams WHERE id=?";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1,team.getId());
			preparedStatement.executeUpdate();
			preparedStatement.close();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	public boolean deleteUsers(User... users) throws DBException {
		String sql = "DELETE FROM users WHERE id=?";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			for (User temp : users){
				preparedStatement.setInt(1, temp.getId());
				preparedStatement.executeUpdate();
			}
			preparedStatement.close();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	public boolean updateTeam(Team team) throws DBException {
		String sql = "UPDATE teams SET name=? WHERE id=?";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1,team.getName());
			preparedStatement.setInt(2, team.getId());
			preparedStatement.executeUpdate();
			preparedStatement.close();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

}
