package com.epam.rd.java.basic.task7.db.entity;

public class Team {

	private int id;

	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static Team createTeam(String name) {
		Team newTeam = new Team();
		newTeam.setName(name);
		return newTeam;
	}
	public boolean equals (Object team){
		if (team == null) {
			return false;
		}
		if (team.getClass() != this.getClass()) { return false;}

		if (name.equals(((Team) team).getName())) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		return name;
	}
}
